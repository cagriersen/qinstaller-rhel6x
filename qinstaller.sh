#!/bin/sh

#LWQ qmail kurulum scripti.
#Author: Cagri Ersen
#cagri.ersen@gmail.com
#Surum: 1.0 (23.12.2012)

##############################################################
#
#  KURULUMA BASLAMADAN ONCE MUTLAKA "README"'DOSYASINI
#  OKUYUNUZ!
#
##############################################################


function OnayAL() {
while true
do
echo -n "[Devam etmek istiyor musunuz ? <Evet/Hayir>] :"
read ONAY
case $ONAY in
Evet|evet|EVET) break ;;
Hayir|HAYIR|hayir)
echo Kurulum sonlandiriliyor...
exit
;;
*) echo Evet ya da Hayir demelisiniz.
esac
done
echo Kuruluma devam ediyoruz.
}

function Tamam () {
echo -e '[ \E[32m'"\033[1mTamam\033[0m ]"
}

FILE="/tmp/out.$$"
GREP="/bin/grep"

echo "
ROOT USER KONTROLU
------------------
Kurulumu yapabilmek icin sisteme root kullanicisi ile login olmalisiniz. 
Once bu durum kontrol edilecek!
" && sleep 1
read -p "Devam etmek icin <ENTER> tusuna basin..."
echo "" && sleep 1

	if [[ $EUID -ne 0 ]]; then

		echo "Root degilsiniz..."
		echo "Lutfen root kullanicisi ile login olduktan sonra scripti yeniden calistiriniz." 1>&2
		echo "Kurulum sonlandiriliyor..." && echo "" && sleep 2
		exit 0  

	else

		echo "Root olarak login durumdasiniz. Kuruluma devam edebiliriz.."
			
	fi

sleep 1

echo "
ONCEDEN KURULU OLMASI GEREKLI PAKETLERIN KONTROLU
-------------

Bu adimda, qmail kurulumunun yapilabilmesi icin sistemde bulunmasi 
gereken wget, patch, gcc ve make gibi paketlerin kurulu olup olmadigi
kontrol edilecek. Minimal olarak kurulmus CentOS sistemlerde bu paketler
yuklu olmadigi icin, boyle bir durum varsa kuruluma baslamadan once bahsi
bahsi gecen paketlerin ayrica yuklenme islemi bu asamada yapilacaktir!
"

sleep 1
read -p "Devam etmek icin <ENTER> tusuna basin..."
echo ""

    echo "check gcc" && sleep 1

	gcc_check=`which gcc`
    
	if [ -x "$gcc_check" ]

	then

		echo "gcc ok --> $gcc_check"
	
	else

		echo "
		UYARI: Sisteminizde gcc paketi kurulu degil.
		Devam edebilmek icin gcc'nin kurulmasi gerekmekte. 

		Simdi yum kullanilarak gcc ve bagimli paketler sisteme yuklenecek!
		" && echo "" && sleep 1
				
		OnayAL

		echo "" && echo "gcc kurulumu baslatiliyor..." && echo ""
		yum -y install gcc
		echo "" && echo "gcc kurulumu tamamlandi!" && echo "" && sleep 2
					
		echo "Simdi bir kez daha gcc kontrolu yapilacak!" && echo "" && sleep 2

		gcc_check=`which gcc`

		if [ -x "$gcc_check" ]

		then

			echo "" && echo "gcc yuklenmis durumda: $gcc_check" && sleep 1 && echo ""

		else

			echo ""
			echo "
			UYARI: Sisteminizde gcc halen yuklu degil
			Lutfen manuel olarak yukleyip scripti yeniden
			calistirin
			 
			Kurulum sonlandiriliyor...
			"
			sleep 2
			exit 0 
		fi
	fi

sleep 1

   echo "check make" && sleep 1

	make_check=`which make`
    
	if [ -x "$make_check" ]

	then

		echo "make ok --> $make_check"
			
	else

		echo "
		UYARI: Sisteminizde make paketi kurulu degil.
		Devam edebilmek icin make'in kurulmasi gerekmekte. 

		Simdi yum kullanilarak make ve (varsa) bagimli paketler sisteme yuklenecek!
		" && echo "" && sleep 1
				
		OnayAL

		echo "" && echo "make kurulumu baslatiliyor..." && echo ""
		yum -y install make
		echo "" && echo "make kurulumu tamamlandi!" && echo "" && sleep 2
					
		echo "Simdi bir kez daha make kontrolu yapilacak!" && echo "" && sleep 2

		make_check=`which make`

		if [ -x "$make_check" ]

		then

			echo "" && echo "make yuklenmis durumda: $make_check" && sleep 1 && echo ""

		else

			echo ""
			echo "
			UYARI: Sisteminizde make halen yuklu degil
			Lutfen manuel olarak yukleyip scripti yeniden
			calistirin
			 
			Kurulum sonlandiriliyor...
			"
			sleep 2
			exit 0 
		fi
	fi

sleep 1

    echo "check patch" && sleep 1

	patch_check=`which patch`
    
	if [ -x "$patch_check" ]

	then

		echo "patch ok --> $patch_check"
			
	else

		echo "
		UYARI: Sisteminizde patch paketi kurulu degil.
		Devam edebilmek icin patch'in kurulmasi gerekmekte. 

		Simdi yum kullanilarak patch ve (varsa) bagimli paketler sisteme yuklenecek!
		" && echo "" && sleep 1
				
		OnayAL

		echo "" && echo "patch kurulumu baslatiliyor..." && echo ""
		yum -y install patch
		echo "" && echo "patch kurulumu tamamlandi!" && echo "" && sleep 2
					
		echo "Simdi bir kez daha patch kontrolu yapilacak!" && echo "" && sleep 2

		patch_check=`which patch`

		if [ -x "$patch_check" ]

		then

			echo "" && echo "patch yuklenmis durumda: $make_check" && sleep 1 && echo ""

		else

			echo ""
			echo "
			UYARI: Sisteminizde patch halen yuklu degil
			Lutfen manuel olarak yukleyip scripti yeniden
			calistirin
			 
			Kurulum sonlandiriliyor...
			"
			sleep 2
			exit 0 
		fi
	fi


sleep 1

    echo "check wget" && sleep 1

	wget_check=`which wget`
    
	if [ -x "$wget_check" ]

	then

		echo "wget ok --> $wget_check"

	else

		echo "
		UYARI: Sisteminizde wget paketi kurulu degil.
		Devam edebilmek icin wget'in kurulmasi gerekmekte. 

		Simdi yum kullanilarak wget ve (varsa) bagimli paketler sisteme yuklenecek!
		" && echo "" && sleep 1
				
		OnayAL

		echo "" && echo "wget kurulumu baslatiliyor..." && echo ""
		yum -y install wget
		echo "" && echo "wget kurulumu tamamlandi!" && echo "" && sleep 2
					
		echo "Simdi bir kez daha wget kontrolu yapilacak!" && echo "" && sleep 2

		wget_check=`which wget`

		if [ -x "$wget_check" ]

		then

			echo "" && echo "wget yuklenmis durumda: $make_check" && sleep 1 && echo ""

		else

			echo ""
			echo "
			UYARI: Sisteminizde wget halen yuklu degil
			Lutfen manuel olarak yukleyip scripti yeniden
			calistirin
			 
			Kurulum sonlandiriliyor...
			"
			sleep 2
			exit 0 
		fi
	fi

sleep 1

#Minimal kurulmus CentOS sistemlerde sendmail kurulu gelmegidinden, bu adimdaki bazi islemler
#hata verir. Ignore edebilirsiniz.

echo "
SENDMAIL'IN DEVRE DISI BIRAKILMASI
----------------------------------
Bu adimda sendmail servisi durdurulacak ve startuptan kaldirilacak.
"
sleep 1
read -p "Devam etmek icin <ENTER> tusuna basin..."
echo ""

	echo "Sendmail durduruluyor."

sendmail_check=`which sendmail`
    
	if [ -x "$sendmail_check" ]

	then
	
			checkedserv='sendmail'
			if ps ax | grep -v grep | grep $checkedserv > /dev/null

			then
			
			service sendmail stop
			sleep 1 && echo ""
		
			echo -n "Sendmail startup'dan kaldiriliyor:" && sleep 1 && chkconfig --level 123456 sendmail off && \
			echo -ne "\t\t\t" && Tamam
			sleep 1
		
			echo -n "Sendmail qmail'e linkleniyor:" && sleep 1 && \
			mv /usr/lib/sendmail /usr/lib/sendmail.old && \
			mv /usr/sbin/sendmail /usr/sbin/sendmail.old && \
			chmod 0 /usr/lib/sendmail.old /usr/sbin/sendmail.old && \
			ln -s /var/qmail/bin/sendmail /usr/lib/sendmail && \
			ln -s /var/qmail/bin/sendmail /usr/sbin/sendmail && \
			echo -ne "\t\t\t\t" && Tamam
			sleep 1		
		
			else

			echo "Sendmail servisi zaten calismiyor..." && sleep 1
			
			fi

	else

	echo "Sendmail zaten kurulu degil..."

	fi
	
sleep 1
	
echo "
DOWNLOADS
----------
Simdiki adimda root (/) dizini altinda (eger yoksa) qmail-downloads
isimli bir klasor olusturacak ve gerekli paketler bu dizine download 
edilecek. 
"  
sleep 1
read -p "Devam etmek icin <ENTER> tusuna basin..."
echo "" && sleep 1

	if [ -d /qmail-downloads ]

		then

			echo "/qmail-downloads dizini mevcut. Dosyalar bu dizine download edilecek!" && echo "" 

		else

			echo "/qmail-downloads dizini mevcut degil!" && sleep 1 && \
			echo -n "mkdir /qmail-downloads:" && sleep 1 && \
			mkdir /qmail-downloads && \
			echo -ne "\t\t\t\t\t" && Tamam
		
	fi

	sleep 1

	echo "" && echo "Paketler download ediliyor..."

			cd /qmail-downloads && sleep 1 && \
			wget -q http://www.syslogs.org/qmail/src/netqmail-1.06.tar.gz -O /qmail-downloads/netqmail-1.06.tar.gz && \
			echo -n "netqmail-1.06.tar.gz:" && sleep 1 && echo -ne "\t\t\t\t\t" && Tamam 

			sleep 1

			wget -q http://www.syslogs.org/qmail/src/ucspi-tcp-0.88.tar.gz -O /qmail-downloads/ucspi-tcp-0.88.tar.gz && \
			echo -n "ucspi-tcp-0.88.tar.gz:" && sleep 1 && echo -ne "\t\t\t\t\t" && Tamam

			sleep 1

			wget -q http://www.syslogs.org/qmail/src/daemontools-0.76.tar.gz -O /qmail-downloads/daemontools-0.76.tar.gz
			echo -n "daemontools-0.76.tar.gz:" && sleep 1 && echo -ne "\t\t\t\t" && Tamam

			sleep 1


echo "
PAKETLERIN KONTROL EDILMESI
-----------------------------
Download islemleri tamamlanmis gorunuyor.

Ancak herhangi bir baglanti sorunu sebebi ile paketler duzgun
olarak download edilemezse ilerki adimlarda kurulum scripti
hata verecektir. Bu nedenle simdi download isleminin sorunsuz
gittiginden emin olmak uzere paketlerin /qmail-downloads dizininde
olup olmadigi ve ebatlarinin dogrulugu kontrol edilecek!
"
			
sleep 1
read -p "Devam etmek icin <ENTER> tusuna basin..."
echo "" && sleep 1

echo "Paketler /qmail-downloads dizininde mevcut mu ?" && echo "" && sleep 1

	if [ -f /qmail-downloads/daemontools-0.76.tar.gz ]

	then

		echo -n "daemontools-0.76.tar.gz" && sleep 1 && echo -ne "\t\t\t\t\t" && Tamam

	else

		echo ""
		echo "DAEMONTOOLS paketi mevcut degil, bu durumda kuruluma devam edemiyoruz.

		Lutfen http://www.syslogs.org/qmail/src/daemontools-0.76.tar.gz adresine
		erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini yeniden calistiriniz.

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 

	fi
	 
sleep 1

	if [ -f /qmail-downloads/ucspi-tcp-0.88.tar.gz ]

	then

		echo -n "ucspi-tcp-0.88.tar.gz" && sleep 1 && echo -ne "\t\t\t\t\t" && Tamam

	else

		echo ""
		echo "UCSPI paketi mevcut degil, bu durumda kuruluma devam edemiyoruz.

		Lutfen http://www.syslogs.org/qmail/src/ucspi-tcp-0.88.tar.gz  adresine
		erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini yeniden calistiriniz.

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 

		fi

sleep 1

	if [ -f /qmail-downloads/netqmail-1.06.tar.gz ]

	then

		echo -n "netqmail-1.06.tar.gz" && sleep 1 && echo -ne "\t\t\t\t\t" && Tamam
			
	else
		
		echo ""
		echo "NETQMAIL paketi mevcut degil, bu durumda kuruluma devam edemiyoruz.

		Lutfen http://www.syslogs.org/qmail/src/netqmail-1.06.tar.gz adresine
		erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini yeniden calistiriniz.

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 
		
	fi

echo "" && sleep 1

echo "Dosyalarin ebatlari dogru mu ?" && echo "" && sleep 1

DTNAME=/qmail-downloads/daemontools-0.76.tar.gz
DTSIZE=$(stat -c%s "$DTNAME")

	if [ $DTSIZE == 36975 ]

	then

		echo -n "daemontools-0.76.tar.gz = ($DTSIZE bytes)" && sleep 1 && echo -ne "\t\t\t" && Tamam

	else

		echo ""
		echo "
		daemontools-0.76.tar.gz paketinin size'i dogrulanamadi. Bu durumda devam
		edemiyoruz. Paketin download edilmesi sirasinda bir sorun olmus olabilir.
		Lutfen http://www.syslogs.org/qmail/src/daemontools-0.76.tar.gz 
		adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini
		yeniden calistiriniz.

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0

	fi

	sleep 1

UCNAME=/qmail-downloads/ucspi-tcp-0.88.tar.gz
UCSIZE=$(stat -c%s "$UCNAME")

	if [ $UCSIZE == 53019 ]

	then

		echo -n "ucspi-tcp-0.88.tar.gz = ($UCSIZE bytes)" && sleep 1 && echo -ne "\t\t\t" && Tamam

	else

		echo ""
		echo "
		ucspi-tcp-0.88.tar.gz paketinin size'i dogrulanamadi. Bu durumda devam
		edemiyoruz. Paketin download edilmesi sirasinda bir sorun olmus olabilir.
		Lutfen http://www.syslogs.org/qmail/src/ucspi-tcp-0.88.tar.gz
		adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini
		yeniden calistiriniz.

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 
			
	fi

sleep 1

NQNAME=/qmail-downloads/netqmail-1.06.tar.gz
NQSIZE=$(stat -c%s "$NQNAME")

	if [ $NQSIZE == 260941 ]

	then

		echo -n "netqmail-1.06.tar.gz = ($NQSIZE bytes)" && sleep 1 && echo -ne "\t\t\t" && Tamam

	else

		echo "
		netqmail-1.06.tar.gz paketinin size'i dogrulanamadi. Bu durumda devam
		edemiyoruz. Paketin download edilmesi sirasinda bir sorun olmus olabilir.
		Lutfen Lutfen http://www.syslogs.org/qmail/src/netqmail-1.06.tar.gz 
		adresine erisiminiz oldugunu kontrol ettikten sonra install.sh scriptini 
		yeniden calistiriniz.

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 
	fi

sleep 1

echo "
UNCOMPRESS ISLEMLERI
--------------------
Bu adimda, indirilen paketler derleme isleminin yapilacagi dizinlere tasinacak 
ve uncompress edilecekler.

netqmail-1.06 ve ucspi-tcp-0.88 paketleri usr/local/src dizinine,
daemontools-0.76.tar.gz ise /package dizinine tasinacak. 

Eger ilgili dizinler sistemde bulunamazsa otomatik olarak olusturulacaklar."

sleep 1
read -p "Devam etmek icin <ENTER> tusuna basin..."
sleep 1

	if [ -d /usr/local/src ]

	then

		echo "" && echo "/usr/local/src dizini mevcut."

	else
	
		echo "/usr/local/src dizini mevcut degil!" && sleep 1 && \
		echo -n "mkdir -p /usr/local/src:" && sleep 1 && \
		mkdir -p /usr/local/src && \
		echo -ne "\t\t\t\t" && Tamam
		
	fi
	
echo "" && sleep 1

	echo -n "netqmail ve ucspi-tcp src dizinine tasiniyor." && sleep 1 && \
	cd /qmail-downloads && mv -f netqmail-1.06.tar.gz ucspi-tcp-0.88.tar.gz /usr/local/src && \
	echo -ne "\t\t" && Tamam
	echo "" && sleep 1

	if [ -d /package ]

	then

		echo "" && echo "/package dizini mevcut."

	else
		
		echo "/package dizini mevcut degil!" && sleep 1 && \
		echo -n "mkdir -p /package:" && sleep 1 && \
		mkdir -p /package && \
		echo -ne "\t\t\t\t\t" && Tamam

	fi

	echo "" && sleep 1
        
	echo -n "daemontools /package dizinine tasiniyor." && sleep 1 && \
	cd /qmail-downloads && mv -f daemontools-0.76.tar.gz /package && \
	echo -ne "\t\t" && Tamam 
	echo "" && sleep 1
        
	echo -n "Dizin izini 1755'e set ediliyor." && sleep 1 && \
	chmod 1755 /package && \
	echo -ne "\t\t\t" && Tamam 
	echo "" && sleep 1

	echo -n "netqmail-1.06.tar.gz uncompress ediliyor." && sleep 1 && \
	cd /usr/local/src && tar xf netqmail-1.06.tar.gz && rm -f netqmail-1.06.tar.gz && \
	echo -ne "\t\t" && Tamam 
	echo "" && sleep 1

	echo -n "ucspi-tcp-0.88.tar.gz uncompress ediliyor."  && sleep 1 && \
	tar xf ucspi-tcp-0.88.tar.gz && rm -f ucspi-tcp-0.88.tar.gz && \
	echo -ne "\t\t" && Tamam 
	echo "" && sleep 1
	
	echo -n "daemontools-0.76.tar.gz uncompress ediliyor." && sleep 1 && \
	cd /package && tar xf daemontools-0.76.tar.gz && rm -f daemontools-0.76.tar.gz
	echo -ne "\t\t" && Tamam 
	echo "" && sleep 1
echo "" && sleep 1
echo "
USER VE GROUPLARIN OLUSTURULMASI
--------------------------------

Bu adimda, qmail dizin ve kullanicilari sisteme eklenecek.

NOT: Daha onceden qmail kurulumu yaptiysaniz ya da bu scripti birden fazla 
kez calistirdiysaniz /var/qmail dizini ve ilgili kullanicilar sistemde
sistemde mevcut durumda olabilirler. Bu nedenle oncelikli olarak sistemde 
/var/qmail dizininin var olup olmadigi ve ilgili kullanicilarin bulunup
bulunmadigi kontrol edilecek.

Yukaridaki kontrollerden hareketle, sisteminizde daha onceden bir qmail kurulumu
saptanirsa, uyari verilerek kurulum sonlandirilacaktir.

Eger bu yonde bir durum yasarsaniz, uyarida belirtilen adimlari uyguladiktan sonra
bu scripti yeniden calistiriniz.
" && echo "" && sleep 1

OnayAL

sleep 1
				
echo "" && echo "Daha onceki qmail kurulumlai kontrol ediliyor!" && echo "" && sleep 1
echo "Sistemde /var/qmail dizini var mi ?" && sleep 1
				
	if [ -d /var/qmail ]
		
	then	
	
		echo "	
		Ooopss...

		/var/qmail dizini mevcut!
		Sisteminizde qmail hali hazirda kurulu olabilir ya da
		bu scripti daha onceden calistirarak qmail kurulumu yapmis
		olabilirsiniz...

		BU DURUMDA YAPABILECEKLERINIZ:
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldirmaniz gerekiyor.

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak
		kurulumu geri aliniz.

		Varolan qmail'i sistemden kaldirdiktan sonra install-qmail.sh siptinini
		yeniden calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"							
		sleep 1
		exit 0 
				
	else
		
		echo "Default qmail kurulumuna rastlanmadi."
		echo "" && echo "Devam edebiliriz." && echo "" && sleep 1

		echo -n "/var/qmail dizini olusturuluyor..." && sleep 1 && \
		mkdir /var/qmail && \
		echo -ne "\t\t\t" && Tamam 
		echo "" && sleep 1

	fi

sleep 1
echo "Grup ve Kullanicilar sisteme ekleniyor!" && echo "" && sleep 1
			
grep "nofiles:" /etc/group
			
	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		nofiles isimli grup sistemde mevcut.!

		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirarak kurulum yapmis 
		olabilirsiniz.... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 
		
	else
		
		groupadd nofiles
		echo -n "group add: nofiles" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi
				
grep "alias:" /etc/passwd
			
	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		alias isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 

	else

		useradd -g nofiles -M -d /var/qmail/alias alias -s /nonexistent
		echo -n "user add: alias" && echo -ne "\t\t\t\t\t\t" && Tamam
		sleep 1

	fi

grep "qmaild:" /etc/passwd

	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmaild isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 

	else

		useradd -g nofiles -M -d /var/qmail qmaild -s /nonexistent
		echo -n "user add: qmaild" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

grep "qmaill:" /etc/passwd

	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmaill isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 

	else

		useradd -g nofiles -M -d /var/qmail qmaill -s /nonexistent
		echo -n "user add: qmaill" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

grep "qmailp:" /etc/passwd

	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmailp isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0 
	
	else

		useradd -g nofiles -M -d /var/qmail qmailp -s /nonexistent
		echo -n "user add: qmailp" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

sleep 1
        
grep "qmail:" /etc/group

	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmail isimli grup sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0

	else

		groupadd qmail
		echo -n "group add: qmail" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

grep "qmailq:" /etc/passwd
			
	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmailq isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0

	else

		useradd -g qmail -M -d /var/qmail qmailq -s /nonexistent
		echo -n "user add: qmailq" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

grep "qmailr:" /etc/passwd

	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmailr isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0

	else

		useradd -g qmail -M -d /var/qmail qmailr -s /nonexistent
		echo -n "user add: qmailr" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

grep "qmails:" /etc/passwd

	if [ $? = 0 ]

	then

		echo "
		Ooopss...
		qmails isimli kullanici sistemde mevcut.!
		Sisteminizde qmail halihazirda kurulu olabilir ya da
		bu scripti daha onceden calistirip qmail kurulumu yapmis
		olabilirsiniz... 

		BU DURUMDA YAPABILECEKLERINIZ: 
		1 - Sisteminizde daha onceden qmail kurulumu yaptiysaniz
		devam edebilmek icin qmail'i manuel olarak kaldiriniz...

		2 - Eger bu scripti daha onceden calistirarak qmail kurulumu
		gerceklestirdiyseniz /qmail-install-scripts dizininde bulunan 
		unconf.sh ve uninstall.sh scriptlerini sirasi ile calistirarak,
		kurulumu geri aliniz.

		Qmail'i sistemden kaldirdiktan sonra yeniden install-qmail.sh 
		calistirabilirsiniz!

		Kurulum sonlandiriliyor...
		"
		sleep 2
		exit 0

	else

		useradd -g qmail -M -d /var/qmail qmails -s /nonexistent
		echo -n "user add: qmails" && echo -ne "\t\t\t\t\t" && Tamam
		sleep 1

	fi

	echo "" && echo "Tum kullanici ve gruplar sisteme eklendi!." && echo "" && sleep 1

echo ""
echo "
NETQMAIL KURULUMU
------------------
Simdi, netqmail-1.06 pakeri derlenerek sisteme kurulacak. qmail
kurulumu sirasinda sunucunuzun tam alan adini girmeniz (FQDN) istenecek.
"

sleep 1

OnayAL

	echo "" && echo "netqmail kurulum islemi baslatiliyor..." && echo ""

	cd /usr/local/src/
	wget http://www.syslogs.org/qmail/patches/dns-response-size.patch	

	cd /usr/local/src/netqmail-1.06
	sleep 1

	patch < /usr/local/src/dns-response-size.patch
	sleep 2
	

	make setup check

	sleep 1

	echo "" && echo "Derleme islemi Tamamlandi!" && echo "" && sleep 2

	echo "Kurulum baslatiliyor...
	
	UYARI: FQDN'i dogru girdiginize emin olunuz...
	"
	echo -n "Lutfen sunucunuzun tam alan adini (FQDN) yazin: "
	read FQDN
	./config-fast $FQDN
					
	sleep 1 && echo "Tamam."&& echo "" && sleep 1

	echo "Netqmail kurulumu tamamlandi!" && echo ""
			
	sleep 1

echo ""
echo "
UCSPI KURULUMU
--------------

Bu adimda, ucspi-tcp-0.88 paketi compile edilerek sisteme yuklenecek.
"

sleep 1

OnayAL

	cd /usr/local/src/ucspi-tcp-0.88
	echo "" && echo "errno patch'i uygulaniyor..."&& echo "" && sleep 1
	patch < /usr/local/src/netqmail-1.06/other-patches/ucspi-tcp-0.88.errno.patch
	sleep 1 && echo "Tamam." && sleep 1

	echo "" && echo "make islemi baslatiliyor..." && sleep 1
	make
	sleep 1 && echo "" && echo "Tamam."

	sleep 1 && echo "" && echo "make setup check islemi baslatiliyor..." && sleep 1
	make setup check
	echo "" && echo "Tamam." && echo "" && sleep 1

	echo "UCSPI kurulumu tamamlandi!" && echo "" && sleep 1


echo ""
echo "
DAEMONTOOLS KURULUMU

Bu adimda, daemontools-0.76 paketi compile edilerek sisteme yuklenecek.
"

sleep 1 

OnayAL

	cd /package/admin/daemontools-0.76
	echo "" && echo "errno patch'i uygulaniyor..." && echo "" && sleep 1
	cd src
	patch < /usr/local/src/netqmail-1.06/other-patches/daemontools-0.76.errno.patch
	sleep 1 && echo "Tamam."

	cd /package/admin/daemontools-0.76

	echo "" && echo "Kurulum baslatiliyor..." && echo ""
	sleep 1
	package/install
	sleep 1 

#RHEL 6'da inittab kullanimindaki degisiklik nedeni ile daemontools'un kendi ekledigi
#satiri kaldirip, startup'i /etc/init dizini altinda olusturdugumuz svscan.conf dosyasinda belirtiyoruz.
sed -i '/svscanboot/ d' /etc/inittab
echo "start on runlevel [345]
respawn
exec /command/svscanboot" > /etc/init/svscan.conf
initctl reload-configuration
initctl start svscan

	echo "" && echo "Daemontools kurulumu tamamlandi!" && echo ""

sleep 2

echo "
KURULUM SONRASI ISLEMLERI
-------------------------
Bu adimda, qmail'in startup configuration islemleri yapilacak.
conf dosyalari download edilecegi icin, internet erisiminizin 
problemsiz olduguna emin olun. Aksi halde kurulum scripti hata
verecektir.

Ilgili conf dosyalari http://www.syslogs.org/qmail adresinde bulunmaktadir."

	sleep 1
	read -p "Devam etmek icin <ENTER> tusuna basin..."

	echo "" && echo -n "/var/qmail/rc indiriliyor betigi..." && sleep 1 && \
	cd /var/qmail && \
	wget -q http://www.syslogs.org/qmail/rc -O /var/qmail/rc && \
	echo -ne "\t\t\t" && Tamam
	echo "" && sleep 1

	echo "rc betigi check ediliyor..." && sleep 1 && echo ""
	echo "rc dosyasi yerinde mi ? " && sleep 1
			
	if [ -f /var/qmail/rc ]

	then
	
		echo -n "/var/qmail/rc ?" && sleep 1 && echo -ne "\t\t\t\t\t\t" && Tamam
		echo "" && sleep 1

	else
		
		sleep 1  && echo ""
		echo "
		Ooopss...
		rc betigi mevcut degil, download sirasinda sorun olusmus olabilir!

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin. 

		1 - http://www.syslogs.org/qmail/rc adresine erisiminiz
		oldugundan emin olun.

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0

	fi
	
echo "rc betiginin ebati dogru mu ?" && sleep 1

rcNAME=/var/qmail/rc
rcSIZE=$(stat -c%s "$rcNAME")

	if [ $rcSIZE == 215 ]

	then

		echo -n "/var/qmail/rc = $rcSIZE byte ?" && sleep 1 && echo -ne "\t\t\t\t" && Tamam
		
	else

		echo ""
		echo "
		Ooopss...

		rc betiginin ebati dogrulanamadi! Download sirasinda bir sorun
		olusmus olabilir. Bu durumda kuruluma devam edemiyoruz.  
		
		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.  

		1 - http://www.syslogs.org/qmail/rc adresine erisiminiz
		oldugundan emin olun.  

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.  

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0 		
		
	fi

echo "" && sleep 1

	echo -n "chmod /var/qmail/rc 755:" && sleep 1 && \
	chmod 755 /var/qmail/rc && \
	echo -ne "\t\t\t\t" && Tamam
	echo "" && sleep 1
	
	echo -n "/var/qmail/log dizini olusturuluyor..." && sleep 1 && \
	mkdir /var/log/qmail && \
	echo -ne "\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "./Maildir yapisi set ediliyor..." && sleep 1 && \
	echo ./Maildir >/var/qmail/control/defaultdelivery && \
	echo -ne "\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "startup scripti(qmailctl) indiriliyor..." && sleep 1 && \
	cd /var/qmail/bin && \
	wget -q http://www.syslogs.org/qmail/qmailctl -O /var/qmail/bin/qmailctl && \
	echo -ne "\t\t" && Tamam
	echo "" && sleep 1

	echo "qmailctl scripti check ediliyor..." && echo "" && sleep 1

	echo "qmailctl dosyasi yerinde mi ? " && sleep 1

	if [ -f /var/qmail/bin/qmailctl ]

	then
	
		echo -n "/var/qmail/bin/qmailctl ?" && sleep 1 && echo -ne "\t\t\t\t" && Tamam
		sleep 1
		
	else

		echo ""
		echo "
		Ooopss...

		qmailctl betigi mevcut degil! Download sirasinda sorun olusmus
		olabilir. Bu durumda kuruluma devam edemiyoruz!

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.

		1 - http://www.syslogs.org/qmail/qmailctl adresine erisiminiz
		oldugundan emin olun. 

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin. && echo 

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0 

	fi
		
echo "" && echo "qmailctl betiginin ebati dogru mu ?"                       
sleep 1 

ctlNAME=/var/qmail/bin/qmailctl
ctlSIZE=$(stat -c%s "$ctlNAME")

	if [ $ctlSIZE == 3008 ]

	then

	echo -n "/var/qmail/bin/qmailctl = $ctlSIZE byte ?" && sleep 1 && echo -ne "\t\t\t" && Tamam

	else
	
		echo ""	
		echo "
		Ooopss... 

		qmailctl betiginin size'i dogrulanamadi! Download sirasinda bir
		sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz. 

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin. 

		1 - http://www.syslogs.org/qmail/qmailctl adresine erisiminiz
		oldugundan emin olun. 

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin. 

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.
		
		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0 

	fi

echo "" && sleep 1

	echo -n "qmailctl set chmod 755:" && sleep 1 && \
	chmod 755 /var/qmail/bin/qmailctl && \
	echo -ne "\t\t\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "qmailctl /usr/bin altina linkleniyor." && sleep 1 && \
	ln -s /var/qmail/bin/qmailctl /usr/bin && \
	echo -ne "\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "supervise/qmail-send/log dizini olusturuluyor." && sleep 1 && \
	mkdir -p /var/qmail/supervise/qmail-send/log && \
	echo -ne "\t\t" && Tamam
	echo "" && sleep 1

	echo -n "supervise/qmail-smtpd/log dizini olusturuluyor." && sleep 1 && \
	mkdir -p /var/qmail/supervise/qmail-smtpd/log && \
	echo -ne "\t\t" && Tamam
	echo "" && sleep 1

	echo -n "supervise/qmail-send/run dosyasi indiriliyor." && sleep 1 && \
	cd /var/qmail/supervise/qmail-send/ && \
	wget -q http://www.syslogs.org/qmail/qmail-send/run -O /var/qmail/supervise/qmail-send/run && \
	echo -ne "\t\t" && Tamam

	echo "" && sleep 1

	echo "supervise/qmail-send/run dosyasi check ediliyor..." && echo ""
	sleep 1
			
	echo "qmail-send/run dosyasi yerinde mi ? "
			
	if [ -f /var/qmail/supervise/qmail-send/run ]

	then
	
	sleep 1
	echo -n "/var/qmail/supervise/qmail-send/run ?" && sleep 1 && echo -ne "\t\t\t" && Tamam
	
	else

		echo ""
		echo "
		Ooopss...  
		qmail-send/run dosyasi mevcut degil! Download sirasinda sorun
		olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!  

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.  

		1 - http://www.syslogs.org/qmail/qmail-send/run adresine erisiminiz
		oldugundan emin olun.  

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.  

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.
		
		Kurulum sonlandiriliyor!"
		sleep 2
		exit 0 

	fi

echo "" && echo "qmail-send/run dosyasinin ebati dogru mu ?"
sleep 1

qsendrunNAME=/var/qmail/supervise/qmail-send/run
qsendrunSIZE=$(stat -c%s "$qsendrunNAME")

	if [ $qsendrunSIZE == 29 ]

	then
	
	echo -n "/var/qmail/supervise/qmail-send/run = $qsendrunSIZE bytes ?" && sleep 1 && echo -ne "\t" && Tamam

	else

		echo "
		Ooopss... 

		qmail-send/run dosyasinin size'i dogrulanamadi! Download sirasinda
		bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz. 

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin. 

		1 - http://www.syslogs.org/qmail/qmail-send/run adresine erisiminiz
		oldugundan emin olun. 

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin. 

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0 

	fi

echo "" && sleep 1

	echo -n "supervise/qmail-send/log/run indiriliyor." && sleep 1 && \
	cd /var/qmail/supervise/qmail-send/log && \
	wget -q http://www.syslogs.org/qmail/qmail-send/log/run -O /var/qmail/supervise/qmail-send/log/run && \
	echo -ne "\t\t" && Tamam
	
	echo "" && sleep 1
	
	echo "supervise/qmail-send/log/run dosyasi check ediliyor." && echo "" && sleep 1

	echo "Dosya yerinde mi ? " && sleep 1
				
		if [ -f /var/qmail/supervise/qmail-send/log/run ]

		then
		
			echo -n "/var/qmail/supervise/qmail-send/log/run ?" && sleep 1 && echo -ne "\t\t" && Tamam
			
		else

			echo "
			Ooopss... 
			 
			qmail-send/log/run dosyasi mevcut degil! Download sirasinda sorun
			olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!  

			Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
			scriptini yeniden calistirin.  

			1 - http://www.syslogs.org/qmail/qmail-send/log/run adresine erisiminiz
			oldugundan emin olun.  

			2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
			calistirarak su ana kadar yapilan islemleri geri alin.  

			3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
			qmail kurulumunu yeniden baslatin.

			Kurulum sonlandiriliyor!
			"
			sleep 1
			exit 1

		fi
			
echo "" && echo "Ebati dogru mu ?"
sleep 1

qsendlogrunNAME=/var/qmail/supervise/qmail-send/log/run
qsendlogrunSIZE=$(stat -c%s "$qsendlogrunNAME")
			
	if [ $qsendlogrunSIZE == 88 ]

	then

		echo -n "/var/qmail/supervise/qmail-send/run = $qsendlogrunSIZE bytes ?" && sleep 1 && echo -ne "\t" && Tamam

	else
	
		echo "
		Ooopss...

		qmail-send/log/run dosyasinin size'i dogrulanamadi! Download sirasinda
		bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz.

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.

		1 - http://www.syslogs.org/qmail/qmail-send/log/run adresine erisiminiz
		oldugundan emin olun.

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
	sleep 2 
	exit 0

	fi

echo "" && sleep 1

	echo -n "/var/qmail/supervise/qmail-smtpd/run dosyasi indiriliyor" && sleep 1 && \
	cd /var/qmail/supervise/qmail-smtpd/ && \
	wget -q http://www.syslogs.org/qmail/qmail-smtpd/run -O /var/qmail/supervise/qmail-smtpd/run && \
	echo -ne "" && Tamam
	echo "" && sleep 1

	echo "qmail-smtpd/run dosyasi check ediliyor..." && sleep 1
	echo "Dosya yerinde mi ? " && sleep 1
	
	if [ -f /var/qmail/supervise/qmail-smtpd/run ]

	then
		
		echo -n "/var/qmail/supervise/qmail-smtpd/run ?" && sleep 1 && echo -ne "\t\t\t" && Tamam

	else

		echo "
		Ooopss...  

		qmail-smtpd/run dosyasi mevcut degil! Download sirasinda sorun
		olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!  

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.  

		1 - http://www.syslogs.org/qmail/qmail-smtpd/run adresine erisiminiz
		oldugundan emin olun.  

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.  

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0
	fi


echo "" && echo "Ebati dogru mu ?" && sleep 1

qsmtpdrunNAME=/var/qmail/supervise/qmail-smtpd/run
qsmtpdrunSIZE=$(stat -c%s "$qsmtpdrunNAME")

	if [ $qsmtpdrunSIZE == 751 ]

	then

		echo -n "/var/qmail/supervise/qmail-smtpd/run = $qsmtpdrunSIZE bytes ?" && sleep 1 && echo -ne "\t" && Tamam

	else

		echo "
		Ooopss...
		 
		qmail-smtpd/run dosyasinin size'i dogrulanamadi! Download sirasinda
		bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz.  

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.  

		1 - http://www.syslogs.org/qmail/qmail-smtpd/run adresine erisiminiz
		oldugundan emin olun.  

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.  

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0 

	fi
			
echo "" && sleep 1

	echo -n "Concurrencyincoming 20'ye set ediliyor!" && sleep 1 && \
	echo 20 > /var/qmail/control/concurrencyincoming && chmod 644 /var/qmail/control/concurrencyincoming && \
	echo -ne "\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "supervise/qmail-smtpd/log/run dosyasi indiriliyor." && sleep 1 && \
	cd /var/qmail/supervise/qmail-smtpd/log && \
	wget -q http://www.syslogs.org/qmail/qmail-smtpd/log/run -O /var/qmail/supervise/qmail-smtpd/log/run && \
	echo -ne "\t" && Tamam
	echo "" && sleep 1

	echo "qmail-smtpd/log/run betigi check ediliyor..."

	echo "" && sleep 1
			
	echo "Betik yerinde mi ? " && sleep 1 

	if [ -f /var/qmail/supervise/qmail-smtpd/log/run ]

	then

		echo -n "/var/qmail/supervise/qmail-smtpd/log/run ?" && sleep 1 && echo -ne "\t\t" && Tamam

	else

		echo "
		Ooopss...

		qmail-smtpd/log/run dosyasi mevcut degil! Download sirasinda sorun
		olusmus olabilir. Bu durumda kuruluma devam edemiyoruz!

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.

		1 - http://www.syslogs.org/qmail/qmail-smtpd/log/run adresine erisiminiz
		oldugundan emin olun.

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini
		calistirarak su ana kadar yapilan islemleri geri alin.

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!"
		sleep 2
		exit 0 

	fi
			
echo "" && echo "Ebati dogru mu ?"
sleep 1

qsmtpdlogrunNAME=/var/qmail/supervise/qmail-smtpd/log/run
qsmtpdlogrunSIZE=$(stat -c%s "$qsmtpdlogrunNAME")

	if [ $qsmtpdlogrunSIZE == 94 ]

	then

		echo -n "/var/qmail/supervise/qmail-smtpd/log/run = $qsmtpdlogrunSIZE bytes ?" && sleep 1 && echo -ne "\t" && Tamam

	else

		echo "
		Ooopss... 

		qmail-smtpd/log/run dosyasinin size'i dogrulanamadi! Download sirasinda
		bir sorun olusmus olabilir. Bu durumda kuruluma devam edemiyoruz.  

		Lutfen asagidaki adimlari izledikten sonra install-qmail.sh
		scriptini yeniden calistirin.  

		1 - http://www.syslogs.org/qmail/qmail-smtpd/log/run adresine erisiminiz
		oldugundan emin olun.  

		2- /qmail-install-scripts dizinindeki uninstall.sh scriptini 
		calistirarak su ana kadar yapilan islemleri geri alin.  

		3 - Son olarak, install-qmail.sh scriptini yeniden calistirarak
		qmail kurulumunu yeniden baslatin.

		Kurulum sonlandiriliyor!
		"
		sleep 2
		exit 0 

	fi
			
echo "" && sleep 1
	
	echo -n "Betik dosyalari calistirilabilir (755) yapiliyor." && sleep 1 && \
	chmod 755 /var/qmail/supervise/qmail-send/run && \
	chmod 755 /var/qmail/supervise/qmail-send/log/run && \
	chmod 755 /var/qmail/supervise/qmail-smtpd/run && \
	chmod 755 /var/qmail/supervise/qmail-smtpd/log/run && \
	echo -ne "\t" && Tamam
	echo "" && sleep 1

	echo -n "smtpd log dizini olusturuluyor." && sleep 1 && \
	mkdir -p /var/log/qmail/smtpd && \
	echo -ne "\t\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "smtpd log set chown qmail." && sleep 1 && \
	chown qmaill /var/log/qmail /var/log/qmail/smtpd && \
	echo -ne "\t\t\t\t" && Tamam
	echo "" && sleep 1

	echo -n "supervise dizini /service dizinine linkleniyor." && sleep 1 && \
	ln -s /var/qmail/supervise/qmail-send /var/qmail/supervise/qmail-smtpd /service && \
	echo -ne "\t\t" && Tamam
	echo "" && sleep 1

	echo -n "localhost'a relay izni veriliyor..." && sleep 1 && \
	echo '127.:allow,RELAYCLIENT=""' >>/etc/tcp.smtp && \
	/var/qmail/bin/qmailctl cdb
	echo "" && sleep 1

	echo "Aliaslar ekleniyor." && echo ""
	echo -n "[Root e-maillerinin gonderilecegi mail adresini girin] :"
	read email

	echo $email > /var/qmail/alias/.qmail-root
	echo "Tamam." && echo ""
	echo "root@sunucu.domain.com mailleri $email adresine gonderilecek." && echo "" && sleep 1

    	echo -n "[postmaster e-maillerinin gonderilecegi mail adresini girin] :"
    	read email

    	echo $email > /var/qmail/alias/.qmail-postmaster
	echo "Tamam." && echo ""
	echo "postmaster@sunucu.domain.com mailleri $email adresine gonderilecek." && echo "" && sleep 1

	echo -n "mailer-daemon ve abuse postmaster'a linkleniyor" && sleep 1 && \
	ln -s .qmail-postmaster /var/qmail/alias/.qmail-mailer-daemon && \
	ln -s .qmail-postmaster /var/qmail/alias/.qmail-abuse && \
	echo -ne "\t\t" && Tamam
	echo "" && sleep 1

	echo -n "Alias dosyalarini izinleri set ediliyor." && sleep 1 && \
	chmod 644 /var/qmail/alias/.qmail-root /var/qmail/alias/.qmail-postmaster && \
	echo -ne "\t\t" && Tamam
	sleep 1

	echo "" && echo "Konfigurasyon Tamamlandi..." && sleep 1
	
	echo "" && echo "Qmail baslatiliyor..." && echo ""
	sleep 1
	exit 0
