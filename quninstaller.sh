#!/bin/bash

function OnayAL() {
while true
do
echo -n "[Devam etmek istiyor musunuz ? <Evet/Hayir>] :"
read ONAY
case $ONAY in
Evet|evet|EVET) break ;;
Hayir|HAYIR|hayir)
echo Sonlandiriliyor...
exit
;;
*) echo Evet ya da Hayir demelisiniz.
esac
done
echo Uninstall islemi baslatiliyor.
}

echo "
UYARI: 
------
Bu script install-qmail.sh scripti ile yapilan qmail kurulumunu
uninstall etmektedir.!
Uninstall islemi sirasinda qmail'e ait tum dizinler /qmail-backUP
dizinine tasinacaktir. Eger sisteminizde bulunan qmail, install-qmail.sh
scripti ile kurulmadi ise bu scripti calistirmayiniz. Aksi taktirde sitemde
problemler meydana gelebilir.

UNINSTALL SIRASINDA YAPILACAK ISLEMLER:
----------------------------------------
1 - Eger yoksa /qmail-backUP isimli bir dizin olusturulacak!
2 - qmail stop edilecek!
3 - svscan isin eklenmis olan init conf kaldirilacak!
4 - /qmail-downloads dizini silinecek!
5 - /usr/local/src/netqmail-1.06 ve /usr/local/src/ucspi-tcp-0.88 dizinleri silinecek!
6 - /var/qmail dizini /qmail-backUP dizinine tasinacak!
7 - qmailctl dosyasina verilen /usr/bin altindaki link kaldirilacak!
7 - /var/log/qmail dizini /qmail-backUP dizinine tasinacak!
8 - /command dizini silinecek!
9 - /service dizini silinecek!
10 - /package dizini silinecek!
11 - /etc/tcp.smtp ve /etc/tcp.smtp.cdb dosyalari /qmail-backUP dizinine tasinacak!
12 - qmail grup ve kullanicilari sistemden silinecek (/etc/mail altindaki dizinler dahil)!
13 - /usr/local/bin altindaki daemontools linkleri silinecek
14 - svscan processi sonlandirilacak!
15 - tcpserver processi sonlandirilacak!
16 - qmail processleri sonlandirilacak!
17 - sendmail yeniden baslatilacak! (optional)
18 - sendmail startupa yeniden eklenecek (optional)

Bu script, yeni kurulmus bir CentOS'a gore hazirlanmistir. 
Eger scripti calistiracaginiz CentOS halihazirda kullanimda ise
bu scriptin move edecegi dosya/dizinler arasinda sistem tarafindan
kullanilan dizin/dosyalar olabilir...
Eger boyle bir durum soz konusu ise scripti calistirmayiniz!
"
echo ""

sleep 1

OnayAL


	if [ -d /qmail-backUP ]

	then

		echo "/qmail-backUP dizini mevcut. dosya/dizinler bu dizine tasinacak!" && echo ""

	else

		echo "/qmail-backUP dizini mevcut degil!" && echo ""
		mkdir /qmail-backUP && sleep 1 
		echo "Dizin olusturuldu. dosya/dizinler bu dizine tasinacak!" && echo ""

	fi

sleep 1 && echo "Qmail durduruluyor..." && echo ""

qmailctl stop
echo "" && sleep 2 

echo "/qmail-downloads dizini siliniyor..." && echo ""
sleep 1

rm -rf /qmail-downloads
sleep 1

echo "Tamam!" && echo "" && sleep 1

echo "/usr/local/src/netqmail ve ucspi dizinleri siliniyor...." && echo ""

rm -rf /usr/local/src/netqmail-1.06
rm -rf /usr/local/src/ucspi-tcp-0.88

sleep 1

echo "Tamam!" && echo "" && sleep 1

echo  "/var/qmail/ dizini backup dizinine tasiniyor..." && echo ""

	if [ -d /qmail-backUP/qmail ]

	then

		rm -rf /qmail-backUP/qmail
		mv -f /var/qmail/ /qmail-backUP/qmail/
		echo "Tamam!" && echo ""

	else

		mv -f /var/qmail/ /qmail-backUP/qmail
		sleep 1
		echo "Tamam!" && echo ""

	fi


sleep 1

echo "qmailctl linki kaldiriliyor..." && echo ""

rm -f /usr/bin/qmailctl

echo "Tamam!" && echo "" && sleep 1


echo "/var/log/qmail dizini backup dizinine tasiniyor..." && echo ""


	if [ -d /qmail-backUP/var-log-qmail/ ]

	then

		rm -rf /qmail-backUP/var-log-qmail
		mv -f /var/log/qmail/ /qmail-backUP/var-log-qmail/
		echo "Tamam!" && echo 

	else

		mv -f /var/log/qmail/ /qmail-backUP/var-log-qmail/
		sleep 1
		echo "Tamam!" && echo "" && sleep 1

	fi

sleep 1

echo "/command dizini siliniyor..." && echo 

rm -rf /command
sleep 1

echo "Tamam!" && echo "" && sleep 1


echo "/service dizini siliniyor..." && echo ""

rm -rf /service

sleep 1

echo "Tamam!" && echo "" && sleep 1

echo "/package dizini siliniyor..." && echo ""

rm -rf /package

sleep 1

echo "Tamam!" && echo "" && sleep 1

echo "/etc/tcp.smtp ve tcp.smtp.cdb kaldiriliyor..." && echo ""
mv -f /etc/tcp.smtp /qmail-backUP/
mv -f /etc/tcp.smtp.cdb /qmail-backUP/

sleep 1

echo "Tamam!" && echo "" && sleep 1

echo "qmail grup ve kullanicilari kaldiriliyor!" && echo ""

sleep 1

userdel -f alias
userdel -f qmaild
userdel -f qmaill
userdel -f qmailp
userdel -f qmailq
userdel -f qmailr
userdel -f qmails
groupdel nofiles
groupdel qmail
rm -rf /var/mail/qmail*
rm -rf /var/mail/alias

echo "Tamam!" && echo "" && sleep 1

echo "/usr/local/bin altindaki daemontools linkleri kaldiriliyor..." && echo ""

sleep 1

find /usr/local/bin -type l | while read f; do if [ ! -e "$f" ]; then rm -f "$f"; fi; done

echo "Tamam!" && echo "" && sleep 2

	echo "svscanboot icin eklenen inittab girdisi kaldiriliyor...."

	sed -i '/svscanboot/ d' /etc/inittab
	rm -f /etc/init/svscan.conf
	initctl reload-configuration

sleep 2

	echo "Tamam!" && echo "" && sleep 2

	echo "svscan processi sonlandiriliyor..." && echo ""

			pid=`/bin/ps -ef | /bin/grep svscan | grep -v grep| awk -F' ' '{print $2}'`;

	if test "$pid" = "" ; then

			echo "svscan zaten calismiyor!" && echo ""

	sleep 1

	else

			kill $pid;
sleep 1
			echo "Tamam!..." && echo ""

	fi

sleep 2

	echo "tcpserver processi sonlandiriliyor..." && echo ""

	pid=`/bin/ps -ef | /bin/grep tcpserver | grep -v grep| awk -F' ' '{print $2}'`;

if test "$pid" = "" ; then

sleep 1

 echo "tcpserver zaten calismiyor!" && echo ""

	sleep 1

else

	kill $pid;
sleep 1
	echo "Tamam!..." && echo ""

fi

sleep 2


	echo "qmail processleri sonlandiriliyor..." && echo ""

			pid=`/bin/ps -ef | /bin/grep qmail | grep -v grep| awk -F' ' '{print $2}'`;

	if test "$pid" = "" ; then

			echo "qmail zaten calismiyor!" && echo ""
			sleep 1
	else

			kill $pid;
sleep 1
			echo "Tamam!..." && echo ""

	fi


sleep 2

echo "Sendmail startup'a eklenip baslatiliyor..." && echo ""

sleep 1

rm -f /usr/sbin/sendmail
mv /usr/sbin/sendmail.old /usr/sbin/sendmail
rm -f /usr/lib/sendmail
mv /usr/lib/sendmail.old /usr/lib/sendmail
chmod 777 /usr/lib/sendmail /usr/sbin/sendmail
chkconfig --level 2345 sendmail on
service sendmail start

sleep 1 && echo ""

echo " Qmail uninstall tamamlandi...." && echo ""

exit 0

